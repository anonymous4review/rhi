This repo contains an R script and data to reproduce the worked example of the **Realized Habitat Index** (as shown in Figure 3) for manuscript *Realized habitat in landscape connectivity assessments*.

Data and code in this repository are intended to facilitate the peer-review purpose only. No license is granted for the distribution or usage of the contents of this repository for any purpose other than this.

At first usage, you will likely want to un-comment the first line of the script, which will install the necessary R libraries.